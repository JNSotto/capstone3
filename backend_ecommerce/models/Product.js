const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	category:{
		type:String, 
		required:[true,'Name of the Category']
	},
	name:{
		type: String,
		required: [true, 'Name is required']
	},
	brand:{
		type:String,
		required:[true, 'Brand Name']
	},
	description:{
		type:String,
		required:[true, 'Description is required']
	},
	price:{
		type:Number,
		required:[true, 'Price is required']
	},
	sold:{
		type:Number,
		default:0
	},
	inStock:{
		type:Number, 
		default:100
	},
	isActive:{
		type:Boolean, 
		default: true
	},
	isFeatured:{
		type:Boolean,
		default:false
	},
	createdOn:{ 
		type:Date,
		default: Date.now
	}
})
module.exports = mongoose.model('Product', productSchema)
