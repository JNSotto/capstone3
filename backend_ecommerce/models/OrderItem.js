const mongoose = require('mongoose')

const orderItemSchema = new mongoose.Schema({

	product:{
		type:mongoose.Schema.Types.ObjectId,
		ref:'Product',
		require:true
	},
	quantity:{
		type:Number,
		default:1
	},
	subTotal:{
		type:Number,
		default:0
	}
});
 
module.exports = mongoose.model('OrderItem', orderItemSchema)