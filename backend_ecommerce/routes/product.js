const express = require('express');
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth")

//Create Product (Admin Only)
router.post("/createProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin === true){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
});

//Retrieve All Products
router.get("/all", (req,res)=>{
	productController.getAllProduct(req.body).then(resultFromController => res.send(resultFromController));
})
//Retrieve All Active Products
router.get("/", (req,res)=>{
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController));
})
//Retrieve All active top product
router.get("/topProduct", (req,res)=>{
	productController.getAllTopProduct().then(resultFromController => res.send(resultFromController));
})
//Get Total Count of Product
router.get("/getCount", (req,res)=>{
	productController.getCountProduct().then(resultFromController => res.send(resultFromController));
})
//Get Count of Active Product
router.get("/getCountActive", (req,res)=>{
	productController.getCountActiveProduct().then(resultFromController => res.send(resultFromController));
})
//Get Count of InActive Product
router.get("/getCountInActive", (req,res)=>{
	productController.getCountInActiveProduct().then(resultFromController => res.send(resultFromController));
})
//Retrieve All active Feature product
router.get("/featureProduct", (req,res)=>{
	productController.getAllFeatureProduct().then(resultFromController => res.send(resultFromController));
})

//Retrieve All Active Products According to Categories 
router.get("/categories/:category", (req,res)=>{
	productController.getAllActiveProductCategory(req.params).then(resultFromController => res.send(resultFromController));
})

//Retrieve All Active Products According to Brands
router.get("/brands/:brand", (req,res)=>{
	productController.getAllActiveProductBrand(req.params).then(resultFromController => res.send(resultFromController));
})

//Retrieve All Active Products According to Categories and Brands
router.get("/categories/:category/brands/:brand", (req,res)=>{
	productController.getAllActiveProductCategoryBrand(req.params).then(resultFromController => res.send(resultFromController));
})

//Retrieve The Specific Product
router.get("/:productId", (req, res)=>{
	productController.getProduct(req.params).then(resultFromController=> res.send(resultFromController));
})

//Update Product Information (ADMIN ONLY)
router.put("/:productId", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	productController.updateProduct(userData,req.params, req.body).then(resultFromController=> res.send(resultFromController));
}) 

//Archive Product (ADMIN ONLY)
router.put("/:productId/archive", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
	productController.archiveProduct(req.params).then(resultFromController=> res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
})
//Featured Product (ADMIN ONLY) 
router.put("/:productId/featured", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
	productController.featuredProduct(req.params).then(resultFromController=> res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
})


module.exports = router;