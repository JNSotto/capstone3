const express = require('express');
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//User Details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Authenticated admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
	userController.promoteAdmin(req.params).then(resultFromController=> res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
})

//User Add To Cart Item (NON-ADMIN ONLY)
router.post("/addToCart",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.addToCart(userData,req.body).then(resultFromController => 
		res.send(resultFromController))

}) 

//User View Cart (NON-ADMIN ONLY)
router.get("/:userId/cart",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.viewCart(userData,req.params).then(resultFromController => 
		res.send(resultFromController))
})

//User Updated Cart (NON-ADMIN ONLY)
router.put("/:orderItemId/cart",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.updateQuantity(userData,req.params,req.body).then(resultFromController => 
		res.send(resultFromController))
})

//User Delete Cart (NON-ADMIN ONLY)
router.delete("/:orderItemId/cart",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){
	userController.deleteCart(req.params).then(resultFromController => 
		res.send(resultFromController))
	}else{
		res.send('Not Authorized')
	}
})
 
//Retrieve authenticated user's orders
router.get("/:userId/myOrder",auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getOrder(userData,req.params).then(resultFromController=> res.send(resultFromController));
	
})

//Retrieve all Orders (ADMIN ONLY)
router.get("/orders", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}else{
		res.send('Not Authorized')
	}
})

router.get("/getCountUser", (req,res)=>{
	userController.getCountUser().then(resultFromController => res.send(resultFromController));
})

router.get("/getCountOrder", (req,res)=>{
	userController.getCountOrder().then(resultFromController => res.send(resultFromController));
})

router.get("/getCountOrderComplete", (req,res)=>{
	userController.getCountOrderComplete().then(resultFromController => res.send(resultFromController));
})

router.get("/getCountOrderPending", (req,res)=>{
	userController.getCountOrderPending().then(resultFromController => res.send(resultFromController));
})

module.exports = router;
