const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const OrderItem = require("../models/OrderItem")
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User registration
module.exports.registerUser = (reqBody) => {
 
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName, 
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		address: reqBody.address,
		mobileNo: reqBody.mobileNo
	})
 
	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})
}

// checking if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;

		} else {

			return false;

		};
	});

};

// get details
module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}

//Authenticated Admin
module.exports.promoteAdmin = (reqParams) => {
console.log(reqParams.userId)
	return User.findById(reqParams.userId).then(result=> {
		if(result.isAdmin === false){

			result.isAdmin = true
			return result.save().then((promote,err) =>{
				if(err){
					return false
				}else{
					return true

				}
			})
		}else{
			return false
		}


	})
}


//Add to Cart
module.exports.addToCart = async(userData,reqBody) =>{
	 
	//user authentication, only user who login will get access
	if(userData.isAdmin === false){	
		//This is for input product and quantity in a orderItem model to send the data to the orderList 
		//I put Promise.all() to forcely fulfill the promises in the function 
		const orderItemsIds = Promise.all(reqBody.orderList.map(async orderItem =>{
			let newOrderItem = new OrderItem({
				customer:reqBody.customer,
				product:orderItem.product,
				quantity:orderItem.quantity
			})
			newOrderItem = await newOrderItem.save();

			return newOrderItem._id;
		})) 

	
	const orderItemsIdsfixed = await orderItemsIds;

		const totalPrices = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','price')
			const totalPrice = orderItem.product.price * orderItem.quantity;
			return totalPrice
		}))
		//the Array.prototype.reduce() method, it reduce the elements into a single value (a,b), (previousValue + currentValue,0) - it sum up all the elements in an array
		const totalPrice = totalPrices.reduce((previousValue, currentValue)=> previousValue + currentValue)
 
		const subPrices = await Promise.all(orderItemsIdsfixed.map(async (itemId) =>{
			const item = await OrderItem.findById(itemId).populate('product','price')
			const subTotalPrice = item.product.price * item.quantity;
			return await OrderItem.findById(itemId).then(result=>{
				result.subTotal = subTotalPrice
				return result.save()
			})
		}))

		//This is for deducting the Stocks of the product, when the user order a product with quantity, quantity is subtacted on the inStock Object of the Product.
		const numStocks = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','inStock')
			const numStock = orderItem.product.inStock - orderItem.quantity;
			return await Product.findById(orderItem.product).then(result=> {
				result.inStock = numStock
				return result.save().then((stock,err) =>{
				if(err){
					return false
				}else{
					stock.sold += orderItem.quantity
					return stock.save()
				}
			})
			})	
			
		}))

		// This is for automatically Archiving the product if the inStock turns to less than zero. 
		const isActiveUpdate = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','inStock')
			return await Product.findById(orderItem.product).then(result=> {
				if(result.inStock <= 0){
					result.isActive = false
					return result.save()
				}
			})	
			
		}))

		//This is the order checkOut which is going to be displayed to the user
		let order = await new Order ({
			customer:reqBody.customer,
			orderList: orderItemsIdsfixed,
			totalAmount: totalPrice
		})

		return await order.save().then((orderCheckedOut, error)=>{
			if(error){
				return false
			} else {
				return true
			}
		})
		
	} else {
		return false
	}
}

//CartView
module.exports.viewCart = async(userData,reqParams) =>{
	if(userData.isAdmin === false){
		return OrderItem.find({customer:reqParams.userId}).populate('product').then(result=> {
			if(result.length === 0){
				return false
			} else {
				return result
			}
		 
		})

	} else {
		return false
	}

}

//Update Quantity of product in Cart
module.exports.updateQuantity = async(userData,reqParams,reqBody) =>{
	if(userData.isAdmin === false){
		let updatedCart = {
			quantity:reqBody.quantity
		}
		return OrderItem.findByIdAndUpdate(reqParams.orderItemId, updatedCart).then((qty,err)=>{
			if(err){
				return false
			} else {
				return true
			}
		})
	} else {
		return false
	}
}

//Delete Product on the cart
module.exports.deleteCart = (reqParams) => {

	return Task.findByIdAndDelete(reqParams.orderItemId).then((removedProduct, err) => {
		if (err){
			return false
		} else {
			return true
		}
	})
}

//Retrieve all Orders (ADMIN ONLY)
module.exports.getAllOrders = () => {
	return Order.find().populate('customer').populate({path:'orderList',populate:'product'}).sort({'purchasedOn': -1}).then(result =>{
		return result;
	})
}

//Get Count User
module.exports.getCountUser = () =>{
	return User.aggregate([{$match: {isAdmin: false}},{$group: {_id:null,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

//Get Count Order 
module.exports.getCountOrder = () =>{
	return Order.aggregate([{$group: {_id:null ,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}
//Get Count Order Complete
module.exports.getCountOrderComplete = () =>{
	return Order.aggregate([{$match: {status:"complete"}},{$group: {_id:null ,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

//Get Count Order Pending
module.exports.getCountOrderPending = () =>{
	return Order.aggregate([{$match: {status:"pending"}},{$group: {_id:null ,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

