const Product = require("../models/Product");

//Create a Product (ADMIN ONLY)
module.exports.addProduct = (reqBody) => {
	
	let newProduct = new Product({
		category:reqBody.category,
		name:reqBody.name,
		brand:reqBody.brand,
		description: reqBody.description,
		price:reqBody.price,
		inStock:reqBody.inStock

	})

	return newProduct.save().then((product,err) =>{
		if(err) {
			return false
		} else {
			return true
		}
	})
} 


//Retrieve all product
module.exports.getAllProduct = () =>{
	return Product.find().sort({'createdOn': -1}).then(result =>{
		return result;
	})

}

//Retrieve all active product
module.exports.getAllActiveProduct = () =>{
	return Product.find({isActive:true}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve all active top product
module.exports.getAllTopProduct = () =>{
	return Product.find({$and:[{sold: {$lte:1000}},{sold: {$gte:100}}]}
).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//get count of product 
module.exports.getCountProduct = () =>{
	return Product.aggregate([{$group: {_id:null, count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

//get count of  active product 
module.exports.getCountActiveProduct = () =>{
	return Product.aggregate([{$match: {isActive:true}},{$group: {_id:null ,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

//get count of inactive product 
module.exports.getCountInActiveProduct = () =>{
	return Product.aggregate([{$match: {isActive:false}},{$group: {_id:null,count:{$sum:1}}}]).then(result =>{
	 return result
	})
}

//Retrieve all active Feature product
module.exports.getAllFeatureProduct = () =>{
	return Product.find({isFeatured:true}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Categories
module.exports.getAllActiveProductCategory = (reqParams) =>{
	return Product.find({isActive:true,category:reqParams.category}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Brands
module.exports.getAllActiveProductBrand = (reqParams) =>{
	return Product.find({isActive:true,brand:reqParams.brand}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Categories and Brands
module.exports.getAllActiveProductCategoryBrand = (reqParams) =>{
	return Product.find({isActive:true,category:reqParams.category,brand:reqParams.brand}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve specific product
module.exports.getProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isActive === true){
			return result

		}else {
			return false
		}
	}) 
}

//Update Product(ADMIN ONLY)
module.exports.updateProduct = async(userData,reqParams,reqBody) =>{
	if(userData.isAdmin === true){
		let updatedProduct = {
			category:reqBody.category,
			brand:reqBody.brand,
			description: reqBody.description,
			name:reqBody.name,
			price:reqBody.price,
			inStock:reqBody.inStock
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product)=>{
			if(err){
				return false
			} else {
				return true
			}  
		}) 
	} else {
		return false
	}
}


//Archiving a Product (ADMIN ONLY)

module.exports.archiveProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isActive === true){

			result.isActive = false
			return result.save().then((archived,err) =>{
				if(err){
					return false
				} else{
					return true

				}
			})

		}else {
			
			result.isActive = true
			return result.save().then((unArchived,err) =>{
				if(err){
					return false
				} else{
					return true

				}
			})
		}


	})
}

//Featured a Product (ADMIN ONLY)

module.exports.featuredProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isFeatured === true){

			result.isFeatured = false
			return result.save().then((archived,err) =>{
				if(err){
					return false
				} else{
					return true

				}
			})

		}else {
			
			result.isFeatured = true
			return result.save().then((unArchived,err) =>{
				if(err){
					return false
				} else{
					return true

				}
			})
		}


	})
}