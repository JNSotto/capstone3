//Module is considered to be an Object, that is why you need to object destruction 
//import {Fragment} from 'react'
import {useState, useEffect}  from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import EditProduct from './components/EditProduct'
import {BrowserRouter as Router} from 'react-router-dom'
import{Route, Switch} from 'react-router-dom'
import Admin from './pages/Admin'
import Cart from './pages/Cart'
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import ProductView from './pages/ProductView'
import Register from './pages/Register'
import './App.css';
import {UserProvider} from './UserContext'


function App(){

  const[user, setUser] = useState({
    id:null,
    isAdmin:null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

useEffect( () => {
    let token = localStorage.getItem('token');
    fetch('http://localhost:4000/users/details', {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return(
    <UserProvider value = {{user,setUser,unsetUser}}>
        <Router>
          <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/" component = {Home}/>
            <Route exact path="/admin" component = {Admin}/>
            <Route exact path="/products" component = {Products}/>
            <Route exact path="/cart" component = {Cart}/>
            <Route exact path="/products/:productId" component = {ProductView}/>
            <Route exact path="/products/admin/:productId" component = {EditProduct}/>
            <Route exact path="/login" component = {Login}/>
            <Route exact path="/Register" component = {Register}/>
            <Route exact path="/logout" component = {Logout}/>
            <Route path="*" component = {ErrorPage}/>
          </Switch>
        </Container>
        </Router>
    </UserProvider>
    )
}

export default App;

