import {useState,useEffect} from 'react'
import {Fragment, useContext} from 'react'
import {Nav, Navbar,NavDropdown,FormControl, Form, Button,Container,Badge} from 'react-bootstrap'
import {FaSearch} from 'react-icons/fa';
import {AiOutlineShoppingCart} from 'react-icons/ai';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'


export default function AppNavbar(){
	const {user} = useContext(UserContext)

	return(		
        <Navbar bg="dark" variant="dark" expand="lg" sticky="top">
          <Container>
            <Navbar.Brand as={Link} to="/" exact>React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mx-auto">
            <Nav.Link as={Link} to="/products" exact>Product</Nav.Link>
            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
           </NavDropdown>
           <NavDropdown title="Dropdown" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
           </NavDropdown>
           </Nav>
              <Nav className="ms-auto">
              {(user.id !== null) ?
                <Nav.Link as={Link} to="/cart" exact><AiOutlineShoppingCart size={25}/><Badge bg="danger" pill>9</Badge></Nav.Link>
                :<Nav.Link></Nav.Link>}
              
                <NavDropdown title="My Account" id="basic-nav-dropdown">
                 {(user.id === null) ?
                  <Fragment>
                  <NavDropdown.Item as={Link} to="/register" exact>Register</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/login" exact>Login</NavDropdown.Item>
                  </Fragment> 
                  : (user.isAdmin !== false) ? 
                    <Fragment>
                    <NavDropdown.Item as={Link} to="/admin" exact>Admin Dashboard</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/logout" exact>Logout</NavDropdown.Item>
                    </Fragment>
                      :
                    <Fragment>
                     <NavDropdown.Item as={Link} to="/UserDashboard" exact>Order History</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/logout" exact>Logout</NavDropdown.Item>
                    </Fragment>
                  }
                      
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
		)
}
