import {useState,useEffect,useContext} from 'react'
import {Card,Table,Button,Form} from 'react-bootstrap'
import {Link,useHistory} from 'react-router-dom'
import UserContext from '../UserContext'

export default function AdminDashBoard({adminProp}){
const {category,brand,name,description,price,_id, sold, inStock,isActive,isFeatured,createdOn} = adminProp
const {user,setUser} = useContext(UserContext)
const [toggle, setToggle] = useState(false)
const history = useHistory()
function archivedToggle(){
	    fetch(`http://localhost:4000/products/${_id}/archive`,{
        method:'PUT',
        headers:{
          Authorization : `Bearer ${localStorage.getItem("token")}`
        }})
      .then(res => res.json())
      .then(retrieve =>{
      	console.log(retrieve)
      	history.push("/admin")
      })
}	
	return(
			
		 		<tr>
					<td>{category}</td>
	              	<td>{name}</td>
	              	<td>{brand}</td>
	              	<td>{description}</td>
	              	<td>{price}</td>
	              	<td>{inStock}</td>
	              	<td>{sold}</td>
	              	<td>{(isActive)? 'Available':'Not Available'}</td>
	              	<td>{(isFeatured)? 'Yes':'No'}</td>
	              	<td>{createdOn}</td>
	              	<td>
	              	{(isActive) ? 
	              	<Link className="btn btn-success" size="sm" onClick={archivedToggle}>Archive</Link> 
	              	: <Link className="btn btn-danger" onClick={archivedToggle}>UnArchive</Link> 
	              	}
	              	
	              		<Link className="btn btn-primary" size="sm" to = {`/products/admin/${_id}`}>Edit</Link>
	              	</td>
				</tr>   
	)
}

					 
