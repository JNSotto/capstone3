import {useState} from 'react'
import {Card,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductCard ({productProp}){
	console.log(productProp)

//Object destructuring, Syntax: const {properties} = propname
const {name,description,price,_id, sold, inStock} = productProp



	return(
			<Col xs={6} md={4} >
		 		<Card id="cardProductParent">
		 			<Card.Body className="cardProduct">
		 				<Card.Title>
		 					<h3>{name}</h3>
		 				</Card.Title>
		 				<Card.Subtitle>
		 					Description:
		 				</Card.Subtitle>
		 				<Card.Text>
		 					{description}
		 				</Card.Text>
		 				<Card.Subtitle>
		 					inStock:
		 				</Card.Subtitle>
		 				<Card.Text>
		 					{inStock}
		 				</Card.Text>		 				
		 				<Card.Subtitle>
		 					Sold: 
		 				</Card.Subtitle>
		 				<Card.Text>
		 					{sold}
		 				</Card.Text>
		 				<Card.Subtitle>
		 					Php {price}
		 				</Card.Subtitle> 		 	
		 				<Link className = "btn btn-primary" to = {`/products/${_id}`}>Details</Link>
		 			</Card.Body>
       			 </Card>
       		</Col>


	)
}