import {useState,useEffect,useContext} from 'react'
import {Card,Table,Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Cart from '../pages/Cart'


export default function CartCard ({cartProp}){
	console.log(cartProp)

	const {product,quantity} = cartProp

	const {user,setUser} = useContext(UserContext)
	const [name, setName] = useState('')
	const [inStock, setInStock] = useState(0)
	const [price, setPrice] = useState(0)

	const [updatedQuantity, setUpdatedQuantity] = useState(quantity)

	console.log(product)

	useEffect(() =>{
		fetch(`http://localhost:4000/products/${product}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data)

			setName(data.name)
			setPrice(data.price)
			setInStock(data.inStock)
		})
	},[product])

function addQuantity(){
	setUpdatedQuantity(noOfQuantity=>Math.min(updatedQuantity + 1,inStock));
}
function minusQuantity() {
	setUpdatedQuantity(noOfQuantity=>Math.max(updatedQuantity - 1,0)); 
}
let subTotal = updatedQuantity * price;

function removeProduct(e){
	const cartData = JSON.parse(localStorage.getItem('cart'))
	console.log(cartData)

const removeCart = cartData.filter((remove) =>{
	if(remove.product === product){
		console.log(remove)
		remove.pop();
	}
	
})
	console.log(cartData)
		
		//localStorage.setItem('cart',JSON.stringify(cartData))

}


	return(
				
				  
				    <tr>
				      <td>{name}</td>
				      <td>Php{price}</td>
				      <td><Link className="btn btn-danger" onClick = {minusQuantity}>-</Link>	
				      <input type="number" value={updatedQuantity}/>
				      <Link className="btn btn-primary" onClick = {addQuantity}>+</Link>		 				
					</td>
				      <td>{subTotal}</td>
				      <td><Link className="btn btn-danger" onClick = {removeProduct}>REMOVE</Link></td>
				    </tr>
				      	  	
	)
}
