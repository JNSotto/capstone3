import {Fragment, useEffect, useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import CartCard from '../components/CartCard'
import UserContext from '../UserContext'

export default function Cart(){

const [addCarts, setAddCarts] = useState([])
const {user} = useContext(UserContext)

useEffect(()=> {

	const data = JSON.parse(localStorage.getItem('cart'))
	setAddCarts(data.map(cart => {
	return(
		<CartCard key = {cart.id} cartProp = {cart}/>
		)
	}))
},[])

	return(
		<Fragment> 
			<h1>Cart:</h1>
			<Table striped bordered hover variant="dark" size="sm">
			  <thead>
				    <tr>
				      <th>Product Name</th>
				      <th>Price</th>
				      <th>Quantity</th>
				      <th>subTotal</th>
				      <th>Delete</th>
				    </tr>
				  </thead>
				  <tbody>
					{addCarts}
			 		<tr>	
					<td colSpan="3"><Link className="btn btn-danger">Check-Out</Link></td>
				    <td colSpan="2">Total:</td>
				    </tr>
			 		</tbody>
			</Table>
		</Fragment>
		)
}

