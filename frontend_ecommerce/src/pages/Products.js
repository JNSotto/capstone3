import {Fragment, useEffect, useState} from 'react'
import {Container,Row} from 'react-bootstrap'
import ProductCard from '../components/ProductCard'

export default function Products(){

const [products, setProducts] = useState([])

useEffect(()=> {
	fetch('http://localhost:4000/products/')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setProducts(data.map(product => {
	return(
		<ProductCard key = {product.id} productProp = {product}/>
		)
		}))
	}) 
},[])

	return(
		<Container>
			<h1>Products</h1>
			<Row>
			{products}
			</Row>
		</Container>
		)
}