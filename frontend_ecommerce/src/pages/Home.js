import {Fragment} from 'react'
import Banner from '../components/Banner'
import FlashSale from '../components/FlashSale'
import TopProduct from '../components/TopProduct'

export default function Home(){
	return(
			<Fragment>
				<Banner/>
				<FlashSale/>
				<TopProduct/>
			</Fragment>
		)
}

