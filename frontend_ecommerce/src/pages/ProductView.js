import {useState, useEffect, useContext,Fragment} from 'react'
import {Card,Container,Row, Col,Button} from 'react-bootstrap'
import{useParams, useHistory, Link,Redirect} from 'react-router-dom'
import UserContext from '../UserContext'

import Swal from 'sweetalert2'


export default function ProductView (){
	const {productId} = useParams()
	const {user,setUser} = useContext(UserContext)
	const history = useHistory()
	const [name, setName] = useState('')
	const [description,setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(1)
	const [inStock, setInStock] = useState(0)
	const [sold, setSold] = useState(0)
	const [cart, setCart] = useState([])

	useEffect(() =>{
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setInStock(data.inStock)
			setSold(data.sold)
		})


	},[productId])

function addQuantity(){
	setQuantity(noOfQuantity=>Math.min(quantity + 1,inStock))
}
function minusQuantity() {
	setQuantity(noOfQuantity=>Math.max(quantity - 1,0)); 
}
const addToCart = (productId) => {
		const newCart = {
			product:productId,
			quantity:quantity
		}
	if(localStorage.getItem('cart')===null){
		localStorage.setItem('cart','[]');
	}

	const oldCart = JSON.parse(localStorage.getItem('cart'));
	oldCart.push(newCart)
	localStorage.setItem('cart', JSON.stringify(oldCart))
		if(localStorage.getItem('cart')!==null){
			Swal.fire({
					title:' Product is Successfully Added!',
					icon:'success',
					text:'Product is Successfully Added to the Cart'
				})
				history.push("/products")
			}
		
	

	}
	return(
			<Container className="mt-5">
				<Row>
					<Col lg ={{span:6, offset:3}}>
						<Card>
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								<Card.Subtitle>InStock:</Card.Subtitle>
								<Card.Text>{inStock}</Card.Text>
								<Card.Subtitle>Sold:</Card.Subtitle>
								<Card.Text>{sold}</Card.Text>
								<Link className="btn btn-primary" onClick = {addQuantity}>+</Link>
								<input type="number" value={quantity}/>
								<Link className="btn btn-danger" onClick = {minusQuantity}>-</Link>


								{((user.id !== null)&&(user.isAdmin===false)) ?
									<Card.Text><Link className="btn btn-primary" onClick = {()=>addToCart(productId)}>Add To Cart</Link></Card.Text>
									:(user.isAdmin!==false)?
									<Fragment>
										<Card.Text>
											If you are an Admin <Link to ="/logout">Log-out</Link> first.
										</Card.Text>
										<Card.Text>
											If not, go to <Link to ="">Homepage</Link>
										</Card.Text>
									</Fragment>
									:
									<Card.Text>
									<Link className = "btn btn-danger" to ="/login">Log in to Buy</Link>
									</Card.Text>
								}
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>

		)
}